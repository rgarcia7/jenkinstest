#!/bin/bash
file="/usr/src/app/index.js"
if [ -f "$file" ]
then
  echo "$file found."
else
  echo "$file not found."
  echo "Copying default index.html..."
  cp /index.js /usr/src/app/index.js
fi
node index.js